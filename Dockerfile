FROM nginx:1.19

COPY dockerfiles/nginx.conf /etc/nginx/nginx.conf
COPY dockerfiles/conf.d/ /etc/nginx/conf.d/

ENTRYPOINT []
CMD ["nginx"]
